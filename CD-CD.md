# Gitlab CI/CD

1. Giải thích về CI - CD

- CI - Continuous Integration (tích hợp liên tục)
  Là quá trình mà code chúng ta được build, test trước khi merged vào repository. Áp dụng với bất kì commit nào vào repository

- CD - Continuous Deployment (triển khai liên tục)
  Là quá trình xảy ra sau CI, đó là triển khai code lên môi trường thật (staging, production), nói cách khác thì nó sẽ deploy lên server

- Quá trình từ khi code của chúng ta bước vào CI-CD và kết thúc. Thường được gọi là 1 pipeline, trong pipeline này thì chúng ta sẽ có nhiều - job, mỗi job làm 1 công việc cụ thể (build image, test image, ...)

- Khi có ai commit hoặc tạo pull request vào repo này thì quá trình CI-CD sẽ được chạy lại để kiểm tra code có đạt đủ yêu cầu hay không?
- Notes: CI-CD chạy sau khi code được commit lên và có sẵn ở repo rồi và nếu quá trình này `FAILED` thì code của lẫn commit đó vẫn được merge vào repo chứ không phải bị huỷ đi.

2. Bắt đầu với Gitlab CI-CD

- Tạo file với tên là .gitlab-ci.yml. Khi commit code lên Gitlab, Gitlab sẽ phát hiện nếu có file này thì quá trình CI - CD sẽ được kích hoạt sau khi code được commit

- Example:

```
image: docker:19

services:
    - docker:dind

stages:
    - build

before_script:
    - docker version
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

build:
    stage: build
    script:
        - docker pull $CI_REGISTRY_IMAGE:latest || true
        - docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest .
        - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
        - docker push $CI_REGISTRY_IMAGE:latest
```

- Giải thích:

  - Khi comit code và có chứa file .gitlab-ci.yml thì quá trình CI-CD sẽ được chạy
  - Gitlab sẽ tạo ra 1 pipeline, pipeline này chính là toàn bộ những gì bên trong file .gitlab-ci.yml, pine này sẽ chứa nhiều jobs, các jobs này sẽ gửi tới Gitlab Runners, mỗi 1 con runner ở đây thì có thể hiểu là 1 worker, chúng sẽ tạo ra 1 môi trường riêng để chạy job của chúng ta, khi kết thúc thì trả lại kết quả cho Gitlab

    - Các biến môi trường trong file này thì trong quá trình CICD, Gitlab sẽ tự inject vào môi trường các biến này (bắt dầu bằng $CI thì sẽ của thằng Gitlab

    - services: ở đây ta sẽ định nghĩa những service cần thiết cho pipeline của chúng ta (ví dụ như mongodb, redis). Na ná như services của docker-compose. Các images này sẽ link tới image image: docker:19 bên trên

    - docker:dind: đầu tiên runner sẽ pull image `docker:19` về chạy lên tạo môi trường để chạy job của chúng ta trong đó. nhưng ở trong môi trường "đó" - môi trường bên trong `docker:19` thì mặc định không thể kết nối với Docker daemon được và phải cần tới sự trợ giúp của `docker-dind`
      - Docker daemon (Docker server) là thứ quản lý tất cả mọi thứ liên quan tới Docker: images, containers, network

- `Continuous Deployment` and `Continuous Delivery`
